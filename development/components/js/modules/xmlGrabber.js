import AddgeneratorDiv from './generatorDiv';

export default class AddxmlGrabber {
	constructor(init) {
		this.selector = document.querySelector(init.selector) || false;
		if (!this.selector) return;
		
		this.devText = '__NULL__';
		this.url = String;
		this.urlparam = String;
		this.pastTable = document.querySelector('.js__pastTable');
		this.pastSymbol = document.querySelector('.js__pastSymbol');
		this.grabxml = document.querySelector('.js__grabxml');
		this.arrayTable = [];
		this.arraySymbol = [];
		this.filePath = String;

		this._input = this.selector.querySelector('INPUT');
		this._label = this.selector.querySelector('LABEL');
		this._button = this.selector.querySelector('BUTTON');
		this._enterPush = this._input;

		this.loader = document.querySelector('.loader__box');

		this.magic = /^[+|\d?][-\s.()/0-9]*$/;
	}

	static info() { console.log('MODULE:', this.name, true); }

	run() {
		if (this.selector) {
			this.constructor.info();
			this.fetchmyFile();
			this.eventHandlers();
		}
	}

	eventHandlers() {
		this._button.addEventListener('click', event => this.handlerPastlabel(event), false);
		this._enterPush.addEventListener('keydown', event => this.handlerPastlabelEnter(event), false);
		this._enterPush.addEventListener('input', event => this.handlerInputRegexp(event), false);
	}

	handlerPastlabel(event) {
		const et = this._input.value || '';
		const prefix = '?XML=' || '?xml=';
		const postfix = '.xml';
		
		if (this._input.value.length === 0) return;
		if (!et.length <= 0) this.url = `${prefix + et + postfix}`;
		else this.url = '/?';

		this._label.textContent = this.url;
		window.history.pushState(null, this.filePath, this.url);

		this.validState();
		this.fetchmyFile();
	}

	handlerPastlabelEnter(event) {
		if (this._enterPush.value.length === 0) return;
		if (event.key === 'Enter') {
			this.handlerPastlabel(event);
			this._enterPush.blur();
		}
	}

	handlerInputRegexp(event) {
		if (!this._enterPush.value.match(/^[a-zA-Z\-_]+$/gm)) {
			this._enterPush.value = this._enterPush.value.slice(0, -1);
		}
	}

	startLoading() {
		const listiner = () => this.loader.classList.remove('hidden');
		setTimeout(listiner, 100);
	}

	successLoading() {
		const listiner = () => this.loader.classList.add('hidden');
		setTimeout(listiner, 100);
	}

	locationSearch() {
		this._label.textContent = window.location.search || this.devText;
	}

	pushTags() {
		this.arrayTable = [{
			task: 'URLSearchParams :: has TEST.xml',
			result: this.resultURLParam
		}];

		const href = this.grabxml.querySelectorAll('A') || 0;
		this.linksHref = [...href].map(f => f.attributes[0].value);

		const hrefID = this.grabxml.querySelectorAll('[ID]') || 0;
		this.linksID = [...hrefID].map(f => f.attributes[0].value);

		this.arrayTable.push({
			task: ' :: Total href="Link" ',
			result: href.length
		});

		this.arrayTable.push({
			task: ' :: Total id="Link" ',
			result: hrefID.length
		});
	}

	cleanPushTags() {
		this.linksHrefClear = this.linksHref.map(f => f.replace('#', ''));
		this.clearArray = this.linksID.filter(f => !this.linksHrefClear.includes(f)).map(String);

		this.arrayTable.push({
			task: ' :: Unlinked id="Link" ',
			result: this.clearArray.length
		});
	}

	sortPushTags() {
		const arrayTagName = [...this.grabxml.querySelectorAll('*')].map(f => f.tagName)
			.sort((a, b) => a.length - b.length);
		this.uniqueArray = [...new Set(arrayTagName)];
	}

	collectTags() {
		const fn = name => [...document.querySelectorAll(name)];
		this.collection = this.uniqueArray.map((f, index) => ({
			tag: f,
			symbolCount: fn(f)
				.map(x => x.textContent.length)
				.reduce((a, b) => a + b),
			noSpace: fn(f)
				.map(x => x.textContent.replace(/ /gm, '').length)
				.reduce((a, b) => a + b)
		}));
	}

	validState() {
		const _url = new URL(window.location.href);
		const searchParam = new URLSearchParams(_url.search);
		this.urlparam = searchParam.get('XML') || searchParam.get('xml') || '/';
		this.filePath = `${this.urlparam}`;
		this.resultURLParam = this.urlparam.includes('test.xml');
	}

	arrayCleaner() {
		this.arrayTable.length = 0;
		[...this.pastTable.children].map((f, index) => (f.remove()));

		this.arraySymbol.length = 0;
		[...this.pastSymbol.children].map((f, index) => (f.remove()));
	}

	fetchmyFile() {
		this.grabxml.innerHTML = '<div class="grab__wait">Please wait...</div>';

		this.startLoading();
		this.validState();

		const url = this.filePath;
		const options = {
			method: 'GET',
			headers: { 'Content-Type': 'application/xml' },
			mode: 'cors',
			cache: 'default'
		};

		fetch(url, options)
			.then(response => response.text())
			.then((data) => {
				this.parsXml = (new window.DOMParser()).parseFromString(data, 'application/xml');
			})
			.then(() => {
				this.serializ = new XMLSerializer().serializeToString(this.parsXml.documentElement);
			})
			.then(() => {
				if (this.serializ.includes('parsererror')) this.grabxml.innerHTML = '<div class="grab__error">Parser Error!</div>';
				else this.grabxml.innerHTML = this.serializ;
			})
			.then(this.locationSearch.bind(this))
			.then(this.arrayCleaner.bind(this))
			.then(this.pushTags.bind(this))
			.then(this.cleanPushTags.bind(this))
			.then(this.sortPushTags.bind(this))
			.then(this.collectTags.bind(this))
			.then(() => {
				const newTableDiv = __enter => new AddgeneratorDiv(__enter);
				newTableDiv({
					enter: this.arrayTable,
					output: this.pastTable,
					prefixClass: 'geturl',
					contentClass: 'table',
					columnOne: Object.keys(this.arrayTable[0])[0],
					columnTwo: Object.keys(this.arrayTable[0])[1]
				});
			})
			.then(() => {
				this.arraySymbol = this.collection;

				const newSymbolDiv = __enter => new AddgeneratorDiv(__enter);
				newSymbolDiv({
					enter: this.arraySymbol,
					output: this.pastSymbol,
					prefixClass: 'geturl',
					contentClass: 'symbol',
					columnOne: Object.keys(this.arraySymbol[0])[0],
					columnTwo: Object.keys(this.arraySymbol[0])[1],
					columnExtra: Object.keys(this.arraySymbol[0])[2]
				});
			})
			.then(this.successLoading.bind(this))
			.catch(onRejected => console.error(this.errorText, `\n${onRejected.message}`));
	}
}
