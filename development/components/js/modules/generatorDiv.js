export default class AddgeneratorDiv {
	constructor(init) {
		this.enter = init.enter || '__NULL__';
		this.output = init.output || '__NULL__';

		this.prefixClass = init.prefixClass || '__NULL__';
		this.contentClass = init.contentClass || '__NULL__';
		
		this.columnOne = init.columnOne || '__NULL__';
		this.columnTwo = init.columnTwo || '__NULL__';
		
		this.columnExtra = init.columnExtra || false;
		
		this.constructor.info();
		this.buildCard();
	}

	static info() { console.log('•• ADDON:', this.name, true); }

	buildCard() {
		this.enter.map((f, index) => {
			const myDiv = document.createElement('DIV');
			myDiv.className = `${this.prefixClass}__${this.contentClass}-group`;

			const myDivItem = document.createElement('DIV');
			myDivItem.className = `${this.prefixClass}__${this.contentClass}-item`;
			myDivItem.innerText = f[this.columnOne];

			const myDivItemTwo = document.createElement('DIV');
			myDivItemTwo.className = `${this.prefixClass}__${this.contentClass}-item`;
			myDivItemTwo.innerText = f[this.columnTwo];

			myDiv.appendChild(myDivItem);
			myDiv.appendChild(myDivItemTwo);
			
			
			if (this.columnExtra) {
				const myDivItemExtra = document.createElement('DIV');
				myDivItemExtra.className = `${this.prefixClass}__${this.contentClass}-item`;
				myDivItemExtra.innerText = f[this.columnExtra];
				
				myDiv.appendChild(myDivItemExtra);
			}
			
			return this.output.appendChild(myDiv);
		});
	}
}
